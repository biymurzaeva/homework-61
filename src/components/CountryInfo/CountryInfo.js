import React, {useCallback, useEffect, useState} from 'react';
import axios from "axios";
import {COUNTRY} from "../../config";
import './CountryInfo.css';

const CountryInfo = ({countryCode}) => {
	const [country, setCountry] = useState(null);
	const [borders, setBorders] = useState([]);
	const [population, setPopulation] = useState(null);

	const NumberFormat = num =>  {
		return Math.abs(num) >= 1.0e+9
			? Math.round(Math.abs(num) / 1.0e+9 ) + " B"
			: Math.abs(num) >= 1.0e+6
				? Math.round(Math.abs(num) / 1.0e+6 ) + " M"
				: Math.abs(num) >= 1.0e+3
					? Math.round(Math.abs(num) / 1.0e+3 ) + " K"
					: Math.abs(num);
	}

	const fetchData = useCallback(async () => {
		if (countryCode === null) return;

		const response = await axios.get(COUNTRY + countryCode);
		setCountry(response.data);

		const countryBordersList = response.data.borders;

		const results = await Promise.all(countryBordersList.map(code => axios.get(COUNTRY + code)));

		setBorders(results);

		const populationFormat = NumberFormat(response.data.population)
		setPopulation(populationFormat);

	}, [countryCode]);

	useEffect(() => {
		fetchData().catch(e => console.error(e));
	}, [fetchData]);

	let countryBorders = null;

	if (borders.length !== 0) {
		countryBorders = (
			<div>Borders with:
				<ul>
					{borders.map(country => (
						<li key={country.data.area}>{country.data.name}</li>
					))}
				</ul>
			</div>
		);
	}

	return country && (
		<div>
			<h1>{country.name}</h1>
			<p>Capital: {country.capital}</p>
			<p>Population: {population}</p>
			<p>Subregion: {country.subregion}</p>
			<img src={country.flag} alt="flag" className="flag"/>
			{countryBorders}
		</div>
	);
};

export default CountryInfo;