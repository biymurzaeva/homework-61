import React from 'react';
import './Country.css';

const Country = props => {
	return (
		<h4
			className={props.active === props.id ? "active" : "inactive"}
			onClick={props.onClick}
		>
			{props.name}
		</h4>
	);
};

export default Country;