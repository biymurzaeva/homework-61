import React from 'react';
import axios from "axios";
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';

axios.defaults.baseURL = ' https://restcountries.eu/';

ReactDOM.render(<App />, document.getElementById('root'));