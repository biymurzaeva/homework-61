import './App.css';
import {useEffect, useState} from "react";
import {COUNTRIES} from "./config";
import axios from "axios";
import Country from "./components/Country/Country";
import CountryInfo from "./components/CountryInfo/CountryInfo";

const App = () =>  {
  const [countries, setCounties] = useState(null);
  const [selectedCountry, setSelectedCountry] = useState(null);
  const [active, setActive] = useState(null);

  useEffect(() => {
    const fetchData = async () => {
      const response = await axios.get(COUNTRIES);
      const countries = response.data;

      setCounties(countries);
    }

    fetchData().catch(e => console.error(e));
  }, []);

  return countries && (
    <div className="App">
      <div className="sidebar">
        {countries.map((country, i)=> (
          <Country
            key={country.alpha3Code}
            name={country.name}
            onClick={() => {
              setSelectedCountry(country.alpha3Code);
              setActive(i)
            }}
            active={active}
            id={i}
          />
        ))}
      </div>
      <div className="content">
        <CountryInfo
          countryCode={selectedCountry}
        />
      </div>
    </div>
  );
}

export default App;
